import * as app from 'tns-core-modules/application';
import { AppDelegate } from './delegate/app-delegate';

if (app.ios) {
  app.ios.delegate = AppDelegate;
}

app.run({ moduleName: 'app-root' });
