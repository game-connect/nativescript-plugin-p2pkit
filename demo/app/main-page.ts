import { Page } from 'tns-core-modules/ui/page';
import { MainViewModel } from './main-view-model';
import { EventData } from 'tns-core-modules/data/observable';

export function pageLoaded(args: EventData) {
  const page = <Page>args.object;
  global.mainPage = page;
  page.bindingContext = new MainViewModel();
}

// export function tryToConnect(args: EventData) {
//     const button = <Button>args.object;
//     button.text = `CONNECT`;

//     let page = <pages.Page>args.object;
//     let p2pKitModel = page.bindingContext;

//     p2pKitModel.toggleDiscovery();
//     // p2pKitModel.startDiscovery();
//     // setTimeout(function(){
//     //     p2pKitModel.stopDiscovery();
//     //     button.text = `FIND DEVICES`;
//     // }, 5000);
// }

// export function updateInfo(args: EventData) {
//     const button = <Button>args.object;
//     button.text = `CONNECT`;

//     let page = <pages.Page>args.object;
//     let p2pKitModel = page.bindingContext;

//     p2pKitModel.toggleDiscovery();
// }

