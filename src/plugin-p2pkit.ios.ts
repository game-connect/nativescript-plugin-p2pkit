import { Common } from './plugin-p2pkit.common';
// import { ios } from 'tns-core-modules/application';
// import * as utils from 'tns-core-modules/utils/utils';

declare const PPKController;
declare const PPKControllerDelegate;
declare const NSObject;

export class PluginP2pkit extends Common {
  public init(apiKey: string = null): void {
    if (typeof PPKController === 'undefined') {
      this.error('PPKController is undefined');
      return;
    }

    if (PPKController.isEnabled()) {
      this.log('P2PKit Already enabled');
      return;
    }

    const PluginP2pkitAppDelegate = NSObject.extend({
      PPKControllerInitialized: () => {
        this.log('PPKControllerInitialized');
      },
      PPKControllerFailedWithError: errorCode => {
        this.log('PPKControllerFailedWithError');
        this.log(errorCode);
      },
      discoveryStateChanged(/* PPKDiscoveryState */ state) {
        this.log('discoveryStateChanged');
        this.log(state);
      },
      peerDiscovered(/* PPKPeer */ peer) {
        this.log('peerDiscovered');
        this.log(peer);
        this.log(peer.peerID);
        this.log(peer.discoveryInfo);
        this.log(this.decode(peer.discoveryInfo));
        this.log(peer.proximityStrength);

        setTimeout(() => {
          PPKController.pushNewDiscoveryInfo('heloagain');
        }, 3000);
        // if let discoveryInfo = peer.discoveryInfo {
        //   let discoveryInfoString = String(data: discoveryInfo, encoding: .utf8)
        //   print('\(peer.peerID) is here with discovery info: \(discoveryInfoString)')
        // }
      },
      peerLost(/* PPKPeer */ peer) {
        this.log('peerLost');
        this.log(peer);
        this.log(peer.peerID);
      },
      discoveryInfoUpdatedForPeer(/* PPKPeer */ peer) {
        this.log('discoveryInfoUpdatedForPeer');
        this.log(peer);
      },
      proximityStrengthChangedForPeer(/* PPKPeer */ peer) {
        this.log('proximityStrengthChangedForPeer');
        this.log(peer);
      },
      messageReceivedFromNearbyPeer(/* NSData */ message, /* PPKPeer */ peer) {
        this.log('messageReceivedFromNearbyPeer');
        this.log(message, peer);
      }
    }, {
        name: 'PluginP2pkitAppDelegate',
        protocols: [PPKControllerDelegate]
      }
    );

    const delegate = PluginP2pkitAppDelegate.new();
    this.log('enableWithConfigurationObserver', apiKey);
    PPKController.enableWithConfigurationObserver(apiKey, delegate);
    this.log('enabled?', PPKController.isEnabled());
    this.log('discoveryInfoMaxSize?', PPKController.getDiscoveryInfoMaxSize());
    this.log('isMessagingAvailableSize?', PPKController.isMessagingAvailable());
    this.log('getMessageMaxSize?', PPKController.getMessageMaxSize());

    // sendMessageToNearbyPeerWithDeliveryStatusBlock(message, peer, (/* PPKMessageDeliveryCode */ deliveryCode) => {
    //   this.log('deliveryCode', deliveryCode);
    // });

    setInterval(() => {
      this.log('discoveryState', PPKController.discoveryState());
    }, 3000);
  }

  public startDiscovery(): void {
    const ranging = PPKController.enableProximityRanging();
    this.log('ranging?', ranging);
    const started = PPKController.startDiscoveryWithDiscoveryInfoStateRestoration(this.encode('aaa' + (new Date)), false);
    this.log('started?', started);

    this.notify({
      eventName: 'started',
      object: this,
    });

    setTimeout(() => {
      try {
        console.log('pushNewDiscoveryInfo heloagain');
        PPKController.pushNewDiscoveryInfo('heloagain');
      } catch (error) {
        console.error('pushNewDiscoveryInfoError', error);
      }
    }, 3000);
  }

  public stopDiscovery(): void {
    const stopped = PPKController.stopDiscovery();
  }
}
