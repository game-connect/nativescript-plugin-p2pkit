import { Common } from './plugin-p2pkit.common';
import * as application from 'tns-core-modules/application';
import * as permissions from "nativescript-permissions";
declare var android;

declare const ch;

export class PluginP2pkit extends Common {
  protected discoveryListener: any;

  public init(apiKey: string = null): void {
    const { P2PKit, P2PKitStatusListener, discovery } = ch.uepaa.p2pkit;
    const { DiscoveryListener } = discovery;

    permissions.requestPermissions(
      [
        android.Manifest.permission.ACCESS_COARSE_LOCATION,
        android.Manifest.permission.ACCESS_FINE_LOCATION
      ], 
      "App Needs The Following permissions")
      .then(()=>{
      console.log("Permission Granted !");
      })
      .catch(()=>{
      console.log("Permission Denied !");
      }
    );

    if (!P2PKit.isEnabled()) {
      this.log('init', apiKey);

      this.discoveryListener = new DiscoveryListener({
        onStateChanged: (/*int*/ state) => {
          this.log('DiscoveryListener', 'State changed: ' + state);
          // ready to start discovery
        },
        onPeerDiscovered: (/*Peer*/ peer) => {
          // p2pkit has been disabled
          this.log('DiscoveryListener', 'Peer discovered: ' + peer.getPeerId() + ' with info: ' + new String(peer.getDiscoveryInfo()));
        },
        onPeerLost: (/*Peer*/ peer) => {
          // an error occured, handle statusResult
          this.log('DiscoveryListener', 'Peer lost: ' + peer.getPeerId());
        },
        onPeerUpdatedDiscoveryInfo: (/*Peer*/ peer) => {
          // an exception was thrown, reenable p2pkit
          this.log('DiscoveryListener', 'Peer updated: ' + peer.getPeerId() + ' with new info: ' + this.decode(peer.getDiscoveryInfo()));
        },
        onProximityStrengthChanged: (/*Peer*/ peer) => {
          // an exception was thrown, reenable p2pkit
          this.log('DiscoveryListener', 'Peer ' + peer.getPeerId() + ' changed proximity strength: ' + peer.getProximityStrength());
        },
      });

      const statusListener = new P2PKitStatusListener({
        onEnabled: () => {
          this.log('PluginP2pkit', 'P2PKitStatusListener.onEnabled');
          // ready to start discovery
        },
        onDisabled: () => {
          // p2pkit has been disabled
          this.log('PluginP2pkit', 'P2PKitStatusListener.onDisabled');
        },
        onError: (/*StatusResult*/ statusResult) => {
          // an error occured, handle statusResult
          this.log('PluginP2pkit', 'P2PKitStatusListener.onError', statusResult.getStatusCode());
        },
        onException: (/*Throwable*/ throwable) => {
          // an exception was thrown, reenable p2pkit
          this.log('PluginP2pkit', 'P2PKitStatusListener.onException', throwable);
        },
      });
  
      try {
        P2PKit.enable(application.android.context, apiKey, statusListener);
      } catch (err) {
        this.log(err);
      }
    }
  }

  public startDiscovery(): void {
    const { P2PKit, discovery } = ch.uepaa.p2pkit;
    const { DiscoveryPowerMode } = discovery;

    this.log('startDiscovery');

    try {
      P2PKit.enableProximityRanging();
      P2PKit.startDiscovery(this.encode('nativescript-plugin-p2pkit'), DiscoveryPowerMode.HIGH_PERFORMANCE, this.discoveryListener)
    } catch (err) {
      this.log(err);
    }
  }

  public stopDiscovery(): void {
    const { P2PKit } = ch.uepaa.p2pkit;

    this.log('stopDiscovery');

    try {
      P2PKit.stopDiscovery();
    } catch (err) {
      this.log(err);
    }
  }
}
