# NativeScript Plugin p2pkit

[nativescript-plugin-p2pkit](https://gitlab.com/game-connect/nativescript-plugin-p2pkit)

p2pkit.io NativeScript Plugin

## Installation

```javascript
tns plugin add nativescript-plugin-p2pkit
```

## Usage

```javascript
// TODO
```

## API

Describe your plugin methods and properties here. See [nativescript-feedback](https://github.com/EddyVerbruggen/nativescript-feedback) for example.

| Property | Default | Description |
| --- | --- | --- |
| some property | property default value | property description, default values, etc.. |
| another property | property default value | property description, default values, etc.. |

## License

Apache License Version 2.0, January 2004
