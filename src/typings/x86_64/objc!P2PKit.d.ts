
declare class PPKController extends NSObject {

	static addObserver(observer: PPKControllerDelegate): void;

	static alloc(): PPKController; // inherited from NSObject

	static disable(): void;

	static discoveryState(): PPKDiscoveryState;

	static enableProximityRanging(): void;

	static enableWithConfigurationObserver(appKey: string, observer: PPKControllerDelegate): void;

	static getDiscoveryInfoMaxSize(): number;

	static getMessageMaxSize(): number;

	static isEnabled(): boolean;

	static isMessagingAvailable(): boolean;

	static myPeerID(): string;

	static new(): PPKController; // inherited from NSObject

	static pushNewDiscoveryInfo(info: NSData): void;

	static removeObserver(observer: PPKControllerDelegate): void;

	static sendMessageToNearbyPeerWithDeliveryStatusBlock(message: NSData, peer: PPKPeer, block: (p1: PPKMessageDeliveryCode) => void): void;

	static startDiscoveryWithDiscoveryInfoStateRestoration(info: NSData, enabled: boolean): void;

	static stopDiscovery(): void;
}

interface PPKControllerDelegate extends NSObjectProtocol {

	PPKControllerFailedWithError?(errorCode: PPKErrorCode): void;

	PPKControllerInitialized?(): void;

	discoveryInfoUpdatedForPeer?(peer: PPKPeer): void;

	discoveryStateChanged?(state: PPKDiscoveryState): void;

	messageReceivedFromNearbyPeer?(message: NSData, peer: PPKPeer): void;

	peerDiscovered?(peer: PPKPeer): void;

	peerLost?(peer: PPKPeer): void;

	proximityStrengthChangedForPeer?(peer: PPKPeer): void;
}
declare var PPKControllerDelegate: {

	prototype: PPKControllerDelegate;
};

declare const enum PPKDiscoveryState {

	Stopped = 0,

	Unsupported = 1,

	Unauthorized = 2,

	Suspended = 3,

	ServerConnectionUnavailable = 4,

	Running = 5
}

declare const enum PPKErrorCode {

	InvalidAppKey = 0,

	IncompatibleClientVersion = 2,

	InvalidBundleId = 5
}

declare const enum PPKMessageDeliveryCode {

	Dispatched = 0,

	MessagingNotAllowed = 1,

	Throttled = 2,

	PeerUnknown = 3,

	ServerConnectionUnavailable = 4,

	DiscoveryNotRunning = 5
}

declare class PPKPeer extends NSObject {

	static alloc(): PPKPeer; // inherited from NSObject

	static new(): PPKPeer; // inherited from NSObject

	readonly discoveryInfo: NSData;

	readonly peerID: string;

	readonly proximityStrength: PPKProximityStrength;
}

declare const enum PPKProximityStrength {

	Unknown = 0,

	ExtremelyWeak = 1,

	Weak = 2,

	Medium = 3,

	Strong = 4,

	Immediate = 5
}
