import { PluginP2pkit, PluginP2pkitPeer } from 'nativescript-plugin-p2pkit';
import { Observable, EventData } from 'tns-core-modules/data/observable';
import { Page } from 'tns-core-modules/ui/page';
import { Button } from 'tns-core-modules/ui/button';
import { ObservableArray } from 'tns-core-modules/data/observable-array/observable-array';

export class MainViewModel extends Observable {
  public isDiscovering = false;
  public peers: ObservableArray<PluginP2pkitPeer> = new ObservableArray();
  private pluginP2pkit: PluginP2pkit;

  constructor() {
    super();

    this.peers.push({
      peerID: 'dummy',
      discoveryInfo: 'helloworld',
      proximityStrength: -1,
    });

    const { p2pkit } = require('./package.json');
    const { apiKey } = p2pkit || { apiKey: '' };

    this.pluginP2pkit = new PluginP2pkit({ debug: true });
    this.pluginP2pkit.init(apiKey);

    this.pluginP2pkit.on('started', () => {
      console.log('p2pkit started!!');
    });

    this.pluginP2pkit.on('peer_discovered', ({ peer }) => {
      console.log('peer_discovered');
      console.dir(peer);
      // const { peerID } = peer;
      // this.peers[peerID] = peer;
      this.peers.push(peer);
    });

    this.pluginP2pkit.on('peer_lost', ({ peer }) => {
      console.log('peer_lost');
      console.dir(peer);
      // const { peerID } = peer;
      // this.peers[peerID] = peer;
      this.peers.push(peer);
    });
  }

  public startDiscovery() {
    console.log(`startDiscovery ${this.isDiscovering ? 'loading' : 'not loading'} (${this.peers.length})`);
    console.log('isDiscovering', this.isDiscovering);
    this.isDiscovering = true;
    this.pluginP2pkit.startDiscovery();
  }

  public stopDiscovery() {
    this.isDiscovering = false;
    console.log('isDiscovering', this.isDiscovering);
    this.pluginP2pkit.stopDiscovery();
  }

  public toggleButton(button) {
    if (this.isDiscovering) {
      button.text = 'FIND DEVICES:';
      this.stopDiscovery();
    } else {
      button.text = 'TRY TO CONNECT...';
      this.startDiscovery();
    }
  }

  public tryToConnect(args: EventData) {
    this.toggleButton(<Button>args.object);
  }

  public onPeerTap({ index, view }) {
    console.log(index, view, arguments.length);
  }
}
