const {
  PluginP2pkit
} = require('nativescript-plugin-p2pkit');
const pluginP2pkit = new PluginP2pkit();

describe('greet function', () => {
  it('exists', () => {
    expect(pluginP2pkit.greet).toBeDefined();
  });

  it('returns a string', () => {
    expect(pluginP2pkit.greet()).toEqual('Hello, NS');
  });
});
