import { Common } from './plugin-p2pkit.common';
export declare class PluginP2pkit extends Common {
  init(apiKey: string): void;
  startDiscovery(): void;
  stopDiscovery(): void;
}
export declare class PluginP2pkitPeer extends Object {
  discoveryInfo: Object;
  peerID: string;
  proximityStrength: number;
}
export declare class StatusResult extends java.lang.Object {
  constructor(statusCode: number);
  equals(o: object): boolean;
  getStatusCode(): number;
  hashCode(): number;
}
