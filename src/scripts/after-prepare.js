const fs = require('fs');
const path = require('path');

const pluginName = path.basename(path.dirname(path.dirname(__dirname)));

fs.scandirSync = (dir, regex) => fs.readdirSync(dir).reduce((files, file) => {
  const name = path.join(dir, file);
  return [...files, ...(fs.statSync(name).isDirectory() ? fs.scandirSync(name, regex) : [name])];
}, []).filter(name => name.match(regex) !== null);

module.exports = (hookArgs) => {
  const { projectDir } = hookArgs.prepareData;

  fs.scandirSync(`${__dirname}/../platforms/ios`, /\.modulemap$/).forEach((modulemap) => {
    console.log('PluginP2pkitHook', 'Running AfterPrepare Hook');

    const matches = modulemap.match('/src(.*)');
    if (matches !== null) {
      const source = projectDir + matches[0].replace('src', `node_modules/${pluginName}`);
      const target = projectDir + matches[1];
      const targetFolder = path.dirname(target);

      if (!fs.existsSync(targetFolder)) {
        fs.mkdirSync(targetFolder);
      }
      fs.copyFileSync(source, target);
      console.log('PluginP2pkitHook', 'AfterPrepare', `File ${source} successfully copied to ${target}`);
    }
  });
};
