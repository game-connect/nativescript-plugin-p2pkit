import { Observable } from 'tns-core-modules/data/observable';
import * as app from 'tns-core-modules/application';
import * as dialogs from 'tns-core-modules/ui/dialogs';

export class Common extends Observable {
  private debug: boolean = false;

  constructor({ debug }) {
    super();
    this.debug = debug;
  }

  public log(type, ...args) {
    if (this.debug) {
      console.log(type, ...args);
    }
  }

  public error(type, ...args) {
    if (this.debug) {
      console.error(type, ...args);
    }
  }

  public encode(str: string) {
    return str.split('').map(char => char.charCodeAt(0));
  }

  public decode(array: Array<number>) {
    return array.map(code => String.fromCharCode(code)).join('');
  }
}
