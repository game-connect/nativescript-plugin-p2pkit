export class AppDelegate extends UIResponder implements UIApplicationDelegate {
  public static ObjCProtocols = [UIApplicationDelegate];
  public static ObjCExposedMethods = {
    // runOnBackground: { returns: interop.types.void }
  };

  public applicationPerformFetchWithCompletionHandler(application: UIApplication, completionHandler: any) {
    console.log('applicationPerformFetchWithCompletionHandler', application);
  }

  public applicationDidEnterBackground(application: UIApplication) {
    console.log('applicationDidEnterBackground', application);
  }

  public applicationDidFinishLaunchingWithOptions(application: UIApplication, launchOptions: NSDictionary<string, any>): boolean {
    console.log('applicationDidFinishLaunchingWithOptions', application, launchOptions);
    return true;
  }
}
